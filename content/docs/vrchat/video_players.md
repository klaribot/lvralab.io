---
weight: 100
title: Video Players
---

# Video Players

AVPro based video players have been notoriously buggy on Linux, knowing to have caused issues such as high memory usage, crashing or just straight out not working. 

We recommend using [GE-Proton](https://github.com/GloriousEggroll/proton-ge-custom), as it contains extra fixes for Wine's Windows Media Foundation implementation that AVPro uses. This will allow you to play regular videos, MPEG-TS and HLS streams, but not RTSP streams.

For playing RTSP streams, you might want to check out [GE-Proton with RTSP patches](https://github.com/SpookySkeletons/proton-ge-rtsp/releases). Sometimes RTSP streams will require 1 or more manual resyncs to start. Be warned that this Proton version is more experimental and may crash your game.

{{< hint info >}}

While more and more live music events are seeing the use of MPEG-TS streams, the majority still utilizes RTSP, and so they can be a pain point for Linux users. If it's a smaller venue, you might have luck asking the organizer to use the MPEG-TS option from VRCDN.

{{< /hint >}}

AVPro on GE is currently expected to be stable, with one excpetion of seeking in the video not currently being implemented, if you encounter game stability issues or other around video playback please report them as soon as possible at the [VRChat proton git issue](https://github.com/ValveSoftware/Proton/issues/1199) or our [community instant messaging](/docs/community/).

## Stream from video player to ffplay/MPV

You may choose to watch RTSP streams on your [WlxOverlay](/docs/steamvr/wlxoverlay/). While audio effects won't work, you will surely not have stability issues.

When in a VRChat world, if a video player fails to play, you can instead run [this script](https://gist.github.com/galister/1a971254af72bb2a5bc27740e984bce2).

You might need to adapt it in case you installed VRChat in another disk and not in your home folder.

It essentially grabs the URL from the VRChat logs and opens it in ffplay (for RTMP streams) or MPV.
